import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

import TableModal from './../TableModal/TableModal';

import './TemtemCard.css';

const TemtemCard = () => {
    const number = useParams().temtemId;

    const [temtemDetail, setTemtemDetail] = useState({});
    const getTemtemDetail = async () => {
        const solicitud = await (await fetch(`https://temtem-api.mael.tech/api/temtems/${number}`)).json();
        setTemtemDetail(solicitud || {});
    };

    const imgURL = "https://temtem-api.mael.tech/"+temtemDetail.lumaIcon;
    useEffect(() => {
        getTemtemDetail();
    }, []);

    let numberTechniques = new Object(temtemDetail.techniques).length;
    let techniques = [];
    for (let index = 0; index < numberTechniques; index++) {
        techniques.push(new Object(temtemDetail.techniques)[index]);
    }

    return(
        <div className='container my-5 border rounded border-light bg-violet'>
            <h1 className='border-bottom border-light pt-2'>{temtemDetail.number} - {temtemDetail.name}</h1>
            <div className='row'>
                <div className='col-sm-12 col-md-3'>
                    <img 
                        src={imgURL} 
                        alt={temtemDetail.name}
                    />
                </div>
                <div className='col-sm-12 col-md-9 border-left border-light'>
                    <p>{temtemDetail.gameDescription}</p>
                    <p className='font-weight-bold'>Height: {new Object(new Object(temtemDetail.details).height).cm}cm</p>
                    <p className='font-weight-bold'>Weight: {new Object(new Object(temtemDetail.details).weight).kg}kg</p>
                    <button className='btn btn-techniques' data-toggle="modal" data-target="#techniqueModal">See all techniques</button>
                </div>
            </div>
        <TableModal
            name = {temtemDetail.name}
            techniques = {techniques}
        />
        </div>
    );
}

export default TemtemCard;