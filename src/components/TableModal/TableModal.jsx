import React from 'react';
import './TableModal.css';

const TableModal = ({name, techniques}) => {
    return (

    <div className="modal fade" id="techniqueModal" tabIndex="-1" role="dialog" aria-labelledby="techniqueModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="techniqueModalLabel">{name} techniques</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    <table className="table">
                        <thead>
                            <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Source</th>
                            <th scope="col">Levels</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                techniques.map((technique, i) => (
                                    <tr key={i}>
                                        <td>{technique.name}</td>
                                        <td>{technique.source}</td>
                                        <td>{technique.levels}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    );
}

export default TableModal;
