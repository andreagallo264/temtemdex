import React from 'react';

const SocialNetworkItem = (props) => {
    return(
        <a
            href={props.url}
            target='_blank'
            rel="noopener noreferrer"
        >
            <img
                src={ props.imgUrl }
                alt={ props.altOpt}
                width='55px'
            />
        </a>
    );
}

export default SocialNetworkItem;