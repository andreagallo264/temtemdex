import React from "react";
import './Home.css';

import SocialNetworkItem from './../SocialNetworkItem/SocialNetworkItem';

import temtemHome from "./../../img/temtemHome.jpg";
import steamicon from './../../img/steam-icon.png';
import twittericon from './../../img/twitter-icon.png';
import instagramicon from './../../img/instagram-icon.png';

const Home = () => {
  return (
    <div className="container mt-4">
      <div className="row text-reverse-div">
        <div className="col-sm-12 col-md-4 border border-dark rounded text-div">
        <h4 className='mt-3 text-center'>
            TemTem on Steam...
        </h4>
          <p className='mt-3 temtem-text text-center'>
            "Temtem is a massively multiplayer creature-collection adventure.
            Seek adventure in the lovely Airborne Archipelago alongside your
            Temtem squad. Catch every Temtem, battle other tamers, customize
            your house, join a friend's adventure or explore the dynamic online
            world."
          </p>

          <div className='d-flex justify-content-around mt-5 social-networks py-2'>
              <SocialNetworkItem
                url='https://store.steampowered.com/app/745920/Temtem/'
                imgUrl={ steamicon }
                altOpt='Steam'
              />

            <SocialNetworkItem
                url='https://twitter.com/playtemtem?lang=es'
                imgUrl={ twittericon }
                altOpt='Twitter'
            />

            <SocialNetworkItem
                url='https://www.instagram.com/playtemtem/'
                imgUrl={ instagramicon }
                altOpt='Instagram'
            />
          </div>
        </div>
        <div className="col-sm-12 col-md-8">
          <img
            src={ temtemHome }
            alt="Temtem game"
            width="100%"
            className='mb-3'
            />
        </div>
      </div>
    </div>
  );
};

export default Home;
