import React from 'react';
import './Footer.css';

const Footer = () => {
    return(
        <footer className='fixed-bottom text-center py-3'>
            <p>Made by Andrea Gallo</p>
        </footer>
    );
}

export default Footer;
