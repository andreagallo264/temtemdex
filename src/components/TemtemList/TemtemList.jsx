import React, { useState, useEffect} from 'react';
import './TemtemList.css';

import TemtemDetail from './../TemtemDetail/TemtemDetail';

const TemtemList = () => {

    const [temtemList, setTemtemList] = useState([]);

    const getTemtems = async () => {
        const solicitud = await (await fetch('https://temtem-api.mael.tech/api/temtems')).json();
        setTemtemList(solicitud.sort((a,b) => a.name.localeCompare(b.name)) || []);
    };

    useEffect(() => {
        getTemtems();
    }, []);

    return(
        <div className='row row-cols-2 row-cols-md-6 row-cols-lg-8 row-cols-xl-12 mt-3'>
            {temtemList.map (temtem => (
                <TemtemDetail
                    temtem = {temtem}
                    key= {temtem.number}
                />
            ))}
        </div>
    );
}

export default TemtemList;
