import React from "react";
import "./NavigationBar.css";
import { Link } from "react-router-dom";

import temtemMenuIcon from "./../../img/temtemMenuIcon.jpg";

const NavigationBar = () => {
  return (
    <nav className="navbar navbar-expand-lg navigation-bar">
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon">
          <img
            src={temtemMenuIcon}
            alt="Menu"
            width="35px"
            className="rounded"
          />
        </span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <img src={temtemMenuIcon} alt="Menu" width="35px" className="rounded mr-3"/>
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active mr-2">
            <Link to="/">Temtem home</Link>
          </li>
          <li className="nav-item mr-2 border-left pl-2">
            <Link to="/temtem">All temtems</Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default NavigationBar;
