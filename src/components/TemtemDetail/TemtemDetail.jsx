import React from 'react';
import './TemtemDetail.css';
import { Link } from 'react-router-dom';

const TemtemDetail = ({temtem}) => {
    const linkRoute = 'temtemdetail/' + temtem.number;
    return(
        <div className='col'>
            <div className="card mx-auto mb-2">
                <img 
                    src={temtem.wikiPortraitUrlLarge} 
                    className="mx-auto d-block mt-2" 
                    alt={temtem.name}
                    height='100'
                    width='100'
                />
                <div className="card-body d-flex flex-column justify-content-center">
                    <h5 className="card-title text-center">{temtem.name}</h5>
                    <button className='btn btn-light text-dark'>
                        <Link to={linkRoute}>Ver más...</Link>
                    </button>
                </div>
            </div>
        </div>
    );
}

export default TemtemDetail;
