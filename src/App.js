import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Home from './components/Home/Home';
import NavigationBar from './components/NavigationBar/NavigationBar';
import TemtemList from './components/TemtemList/TemtemList';
import TemtemCard from './components/TemtemCard/TemtemCard';
import Footer from './components/Footer/Footer';

function App() {
  return (
    <Router>
      <NavigationBar/>

      <Switch>
        <Route path='/temtem'>
          <TemtemList/>
        </Route>
        <Route path='/temtemdetail/:temtemId'>
          <TemtemCard/>
        </Route>
        <Route path='/'>
          <Home/>
        </Route>
      </Switch>

      <Footer/>
    </Router>
  );
}

export default App;
